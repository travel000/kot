<?php

require_once __DIR__ . '/../autoload.php';
use Kot\Lib\Workers;

try {
	Workers::run( 1 );
} catch (Exception $e) {
	echo $e->getMessage()."\n";
}