<?php


namespace Kot\Lib;

/**
 * Class Db.
 *
 * @package Kot\Lib
 */
class Db
{
	
    public static function get()
    {
        global $dbh;
        return $dbh;
    }
}