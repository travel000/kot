<?php


namespace Kot\Lib;

use Kot\Lib\Products;
use Kot\Lib\Db;
use PDO;
use Exception;

/**
 * Class Workers.
 *
 * @package Kot\Lib
 */
class Workers
{

    /**
     */
    public static function run( $type_id = 1 )
    {
        if ( $type_id == 1 ) {
            self::worker_1();            
        }
    }

    private static function worker_1($limit = 10) {
        $dbh = Db::get();

        // получаем данные из очерди
        $sql = '
            SELECT * FROM k_queue q
            WHERE q.status_id = 0
            LIMIT '.$limit.'
        ';
        $stmt = $dbh->prepare( $sql );

        if ( $stmt->execute( array( $limit )   ) ) {
            $rows = $stmt->fetchAll( PDO::FETCH_ASSOC );

            $ids = array_map(function( $v ){
                return $v['id'];
            }, $rows);

            // ставим всем данным статус в работе
            $sql = '
                UPDATE k_queue q
                SET q.`status_id` = 1
                WHERE q.id IN ( '.implode(",", array_fill(0, count($ids), '?')).' )
            ';
            $stmt = $dbh->prepare( $sql );
            if ( $stmt->execute( $ids ) ) {
                if ( $stmt->rowCount( ) ) {
                    // 
                    foreach ($rows as $row) {
                        if ( $_data = json_decode($row['data'], true) ) {

                            $sql = '
                                UPDATE k_queue q
                                SET q.`status_id` = ?
                                WHERE q.id = ?
                            ';
                            $stmt = $dbh->prepare( $sql );

                            try {
                                Products::save( $_data );
                                if ( $stmt->execute( array( 2, $row['id'] ) ) ) {
                                    // 
                                }  
                            } catch (Exception $e) {
                                if ( $stmt->execute( array( 3, $row['id'] ) ) ) {
                                    // 
                                }
                                throw new Exception( "queue_id:{$row['id']} ".$e->getMessage() ); 
                            }
                            
                        }
                    }
                }
            }

        }

    }
}
