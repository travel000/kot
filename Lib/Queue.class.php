<?php


namespace Kot\Lib;

use Kot\Lib\Db;
use PDO;
use Exception;

/**
 * Class Queue.
 *
 * @package Kot\Lib
 */
class Queue
{

    /**
     * Сохраняет очередь.
     * @return bool
     */
    public static function add( $json, $type_id = 1 )
    {
        $dbh = Db::get();
        $dbh->beginTransaction();

        if ( $_data = json_decode($json, true) ) {
            $sql = 'INSERT INTO k_queue (`data`, `type_id`, `created_at`) VALUES '.implode(",", array_fill(0, count($_data), '(?, ?, NOW())'));
            $_values = [];
            foreach ($_data as $row) {
                $_values[] = json_encode($row);
                $_values[] = $type_id;
            }

            $stmt = $dbh->prepare( $sql );
            if ( $stmt->execute( $_values ) ) {
                if ( $dbh->commit() ) {
                    return true;
                }
            }
        }

        $dbh->rollBack();
        return false;;
    }
}
