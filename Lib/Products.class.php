<?php

namespace Kot\Lib;

use Kot\Lib\Db;
use PDO;
use Exception;

/**
 * 
 */
class Products
{
	private static $dbh;

	public static function save( $data ) {

        self::$dbh = Db::get();

        if ( isset($data['product_id']) && preg_match('#^\d+$#', $data['product_id'])  ) {
            $data = self::saveProduct( $data );
            $regionsByKey = self::getRegions( $data );
            if ( @$data['prices'] ) {
                foreach ($data['prices'] as $key => $value) {
                    $_price = [];
                    $_price['product_id'] = $data['_product_id'];
                    $_price['region_id'] = @$regionsByKey[ $key ]['id'];
                    $_price['purchase'] = $value['price_purchase'];
                    $_price['selling'] = $value['price_selling'];
                    $_price['discount'] = $value['price_discount'];
                    self::savePrice( $_price );
                }
            }            
        } else {
            throw new Exception( "product_id is not valid or empty" ); 
        }

	}

    private static function savePrice( $price ) {
        $sql = 'SELECT * FROM k_prices p WHERE p.`product_id` = ? AND p.`region_id` = ?';
        $stmt = self::$dbh->prepare( $sql );
        if ( $stmt->execute( array( $price['product_id'], $price['region_id'] ) ) ) {
            $row = $stmt->fetch( PDO::FETCH_ASSOC );
            if ( $row ) {
                if (
                    $row['purchase'] != $price['purchase']
                    || $row['selling'] != $price['selling']
                    || $row['discount'] != $price['discount']
                ) {
                    $sql = 'UPDATE k_prices p SET p.`purchase` = ?, p.`selling` = ?, p.`discount` = ? WHERE p.`id` = ?';
                    $stmt = self::$dbh->prepare( $sql );
                    if ( $stmt->execute( array( $price['purchase'], $price['selling'], $price['discount'], $row['id'] ) ) ) {
                        if ( $stmt->rowCount( ) ) {
                            return true;
                        }
                    }
                }
            } else {
                $sql = 'INSERT INTO k_prices( `product_id`, `region_id`, `purchase`, `selling`, `discount`, `created_at` ) VALUES ( ?, ?, ?, ?, ?, NOW() )';
                $stmt = self::$dbh->prepare( $sql );
                if ( $stmt->execute( array( $price['product_id'], $price['region_id'], $price['purchase'], $price['selling'], $price['discount'] ) ) ) {
                    if ( self::$dbh->lastInsertId() ) {
                        return true;
                    } else {
                        throw new Exception( "prices not save" );
                    }
                }
            }
        }
        return false;
    }

	private static function saveProduct( $data ) {
        // получаем внутренний id продукта
        $data['_product_id'] = null;
        $sql = 'SELECT p.`id` FROM k_products p WHERE p.`key` = ?';
        $stmt = self::$dbh->prepare( $sql );
        if ( $stmt->execute( array( $data['product_id'] ) ) ) {
            $data['_product_id'] = $stmt->fetchColumn( );
        }
        if ( !@$_data['_product_id'] ) {
            $sql = 'INSERT INTO k_products( `key`, `created_at` ) VALUES ( ?, NOW() )';
            $stmt = self::$dbh->prepare( $sql );
            if ( $stmt->execute( array( $data['product_id'] ) ) ) {
                $data['_product_id'] = self::$dbh->lastInsertId();
            }
        }

        if ( !@$data['_product_id'] ) {
            throw new Exception( "product not save" );
        }

        return $data;
	}

    private static function getRegions( $data ) {
        $regionsByKey = [];
        $keys = [];
        if ( @$data['prices'] ) {
            foreach ($data['prices'] as $key => $value) {
                $keys[] = $key;
            }
        }
        if ( count($keys) > 0 ) {
            $sql = 'SELECT * FROM k_regions r WHERE r.`key` IN ( '.implode(",", array_fill(0, count($keys), '?')).' )';
            $stmt = self::$dbh->prepare( $sql );
            if ( $stmt->execute( $keys ) ) {
                $rows = $stmt->fetchAll( PDO::FETCH_ASSOC );
                if ( $rows ) {
                    foreach ($rows as $row) {
                        $regionsByKey[ $row['key'] ] = $row;
                    }
                }                
            }

            $_need_to_add = [];
            foreach ($keys as $key) {
                if ( !@$regionsByKey[ $key ] ) {
                    $_need_to_add[] = $key;
                }
            }

            if ( count( $_need_to_add ) > 0 ) {
                $sql = 'INSERT INTO k_regions( `key`, `created_at`) VALUES '.implode(",", array_fill(0, count($_need_to_add), '(?, NOW())'));
                $stmt = self::$dbh->prepare( $sql );
                if ( $stmt->execute( $_need_to_add ) ) {
                    $sql = 'SELECT * FROM k_regions r WHERE r.`key` IN ( '.implode(",", array_fill(0, count($keys), '?')).' )';
                    $stmt = self::$dbh->prepare( $sql );
                    $rows = $stmt->fetchAll( PDO::FETCH_ASSOC );
                    if ( $rows ) {
                        foreach ($rows as $row) {
                            $regionsByKey[ $row['key'] ] = $row;
                        }
                    }
                }
            }

        }

        return $regionsByKey;
    }
}