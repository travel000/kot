<?php

require_once __DIR__ . '/../autoload.php';

use Kot\Lib\Queue;

$request_body = file_get_contents('php://input');
$data = json_decode($request_body, true);

// $data = file_get_contents('data.json');

$output = [];
$output['success'] = false;

if ( Queue::add( $data ) ) {
	$output['success'] = true;
}

echo json_encode($output);