<?php

include_once __DIR__ . '/connect.php';

function __autoload($className) {
	$path = explode('\\', $className);

	if( count($path) > 0 ) {
	    if (strtolower($path[0]) == 'kot') {
	        $path = array_slice($path, 1);
	        $path = __DIR__ . '/' . implode('/', $path) . '.class.php';
	        if ( file_exists($path) ) {
	            require_once $path;
	            return true;
	        }
	    }

	}

	return false;
}